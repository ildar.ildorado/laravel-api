<?php

namespace Database\Factories;

use App\Models\Comments;
use App\Models\Posts;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Comments>
 */
class CommentsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'user_id' => User::first()->id,
            'post_id' => Posts::first()->id,
            'description' => Str::random(25)
        ];
    }

    protected $model = Comments::class;
}
