<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Itachi',
            'email' => 'itachi@gmail.com',
            'password' => Hash::make('qwerty'),
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('users')->insert([
            'name' => 'Madara',
            'email' => 'madara@gmail.com',
            'password' => Hash::make('qwerty'),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
