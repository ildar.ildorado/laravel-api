<?php

namespace Tests\Unit;

use App\Models\Comments;
use App\Models\Posts;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Str;
use Tests\TestCase;
use App\Models\User;
use Laravel\Sanctum\Sanctum;

class CommentsTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var User user1
     */
    protected $user1;
    /**
     * @var Posts post1
     */
    protected $post1;
    /**
     * @var User user2
     */
    protected $user2;
    /**
     * @var Posts post2
     */
    protected $post2;

    public function setUp(): void
    {
        parent::setUp();

        $this->user1 = User::factory()->create();
        $this->user2 = User::factory()->create();

        $this->post1 = Posts::factory()
            ->create([
                'user_id' => $this->user1->id
            ]);
        $this->post2 = Posts::factory()
            ->create([
                'user_id' => $this->user2->id
            ]);
    }

    /**
     * @test
     *
     * @throws
     *
     * @return void
     */
    public function successCreateComments()
    {
        Sanctum::actingAs(
            $this->user1,
            ['*']
        );

        $data1 = [
            'description' => Str::random(12),
            'user_id' => $this->user1->id,
            'post_id' => $this->post1->id
        ];

        $response = $this->postJson("/api/comments", $data1);

        $response->assertStatus(201);

        $data2 = [
            'description' => Str::random(12),
            'user_id' => $this->user1->id,
            'post_id' => $this->post2->id
        ];

        $response = $this->postJson("/api/comments", $data2);

        $response->assertStatus(201);
    }

    /**
     * We auth with user A, but trying to create comment as user B
     * @test
     *
     * @throws
     *
     * @return void
     */
    public function failCreatingCommentWithOtherUserId()
    {
        Sanctum::actingAs(
            $this->user1,
            ['*']
        );

        $data = [
            'description' => Str::random(12),
            'user_id' => $this->user2->id,
            'post_id' => $this->post1->id
        ];

        $response = $this->postJson("/api/comments", $data);

        $response->assertStatus(404);
    }

    /**
     * @test
     *
     * @throws
     *
     * @return void
     */
    public function successUpdateComment()
    {
        Sanctum::actingAs(
            $this->user1,
            ['*']
        );

        $comment = Comments::factory()
            ->create([
                'description' => Str::random(12),
                'user_id' => $this->user1->id,
                'post_id' => $this->post1->id
            ]);

        $newDescription = Str::random(12);

        $data = [
            'description' => $newDescription,
            'user_id' => $comment->user_id,
            'post_id' =>  $comment->post_id
        ];

        $response = $this->postJson("/api/comments/{$comment->id}", $data);

        $response->assertStatus(200);

        $comment->refresh();

        $comment = $response->decodeResponseJson()['data'];

        $this->assertEquals($comment['description'], $newDescription);
    }

    /**
     * @test
     *
     * @throws
     *
     * @return void
     */
    public function failUpdatingOtherUsersComment()
    {
        Sanctum::actingAs(
            $this->user1,
            ['*']
        );

        $data = [
            'description' => Str::random(12),
            'user_id' => $this->user2->id,
            'post_id' => $this->post1->id
        ];

        $comment = Comments::factory()
            ->create($data);

        $data['description'] = Str::random(12);

        $response = $this->postJson("/api/comments/{$comment->id}", $data);

        $response->assertStatus(404);
    }

    /**
     * @test
     *
     * @throws
     *
     * @return void
     */
    public function removeComment()
    {
        Sanctum::actingAs(
            $this->user1,
            ['*']
        );

        $data = [
            'description' => Str::random(12),
            'user_id' => $this->user1->id,
            'post_id' => $this->post1->id
        ];

        $comment = Comments::factory()
            ->create($data);

        $response = $this->call('DELETE', "/api/comments/{$comment->id}");

        $response->assertStatus(200);

        $this->assertDatabaseMissing('comments', [
            'id' => $comment->id,
            'user_id' => $this->user1->id,
            'post_id' => $this->post1->id
        ]);
    }

    /**
     * Prevent removing other user's comments
     * @test
     *
     * @throws
     *
     * @return void
     */
    public function failRemovingComment()
    {
        Sanctum::actingAs(
            $this->user1,
            ['*']
        );

        $data = [
            'description' => Str::random(12),
            'user_id' => $this->user2->id,
            'post_id' => $this->post1->id
        ];

        $comment = Comments::factory()
            ->create($data);

        $response = $this->call('DELETE', "/api/comments/{$comment->id}");

        $response->assertStatus(404);
    }
}
