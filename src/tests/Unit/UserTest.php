<?php

namespace Tests\Unit;

use App\Models\Comments;
use App\Models\Posts;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Models\User;
use Laravel\Sanctum\Sanctum;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var User user1
     */
    protected $user1;
    /**
     * @var User user2
     */
    protected $user2;

    public function setUp(): void
    {
        parent::setUp();

        User::truncate();

        $this->user1 = User::factory()
            ->has(Posts::factory()
                ->has(Comments::factory()
                    ->count(2), 'comments')
                ->count(2), 'posts')
            ->create();

        $this->user2 = User::factory()
            ->has(Posts::factory()
                ->has(Comments::factory()
                    ->count(2), 'comments')
                ->count(2), 'posts')
            ->create();
    }

    /**
     * @test
     *
     * @throws
     *
     * @return void
     */
    public function successGettingUsersWithPostsAndComments()
    {
        Sanctum::actingAs(
            $this->user1,
            ['*']
        );

        $response = $this->get('/api/users');

        $users = $response->decodeResponseJson()['data'];

        $this->assertEquals(count($users), 2);

        foreach ($users as $user) {
            $this->assertEquals(count($user['posts']), 2);

            foreach ($user['posts'] as $posts) {
                $this->assertEquals(count($posts['comments']), 2);
            }
        }

        $response->assertStatus(200);
    }
}
