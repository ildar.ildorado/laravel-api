<?php

namespace Tests\Unit;

use App\Models\Posts;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Str;
use Tests\TestCase;
use App\Models\User;
use Laravel\Sanctum\Sanctum;

class PostsTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var User user1
     */
    protected $user1;
    /**
     * @var User user2
     */
    protected $user2;

    public function setUp(): void
    {
        parent::setUp();

        $this->user1 = User::factory()->create();
        $this->user2 = User::factory()->create();
    }

    /**
     * @test
     *
     * @throws
     *
     * @return void
     */
    public function successCreatePosts()
    {
        Sanctum::actingAs(
            $this->user1,
            ['*']
        );

        $posts = Posts::factory()
            ->create([
                'user_id' => $this->user1->id
            ]);

        $data = [
            'title' => Str::random(12),
            'description' => $posts->description,
            'user_id' => $posts->user_id,
        ];

        $response = $this->postJson("/api/posts", $data);

        $response->assertStatus(201);
    }

    /**
     * We get 404 if we try to make unauthorized actions
     * @test
     *
     * @throws
     *
     * @return void
     */
    public function failCreatingPostsWithOtherUserId()
    {
        Sanctum::actingAs(
            $this->user1,
            ['*']
        );

        $post = Posts::factory()
            ->create([
                'user_id' => $this->user2->id
            ]);

        $data = [
            'title' => Str::random(12),
            'description' => $post->description,
            'user_id' => $post->user_id,
        ];

        $response = $this->postJson("/api/posts", $data);

        $response->assertStatus(404);
    }

    /**
     * We auth with user A credentials but trying to update user B post.
     * @test
     *
     * @throws
     *
     * @return void
     */
    public function failUpdatingOtherUserPosts()
    {
        Sanctum::actingAs(
            $this->user1,
            ['*']
        );

        $post = Posts::factory()
            ->create([
                'user_id' => $this->user2->id
            ]);

        $data = [
            'title' => Str::random(12),
            'description' => $post->description,
            'user_id' => $post->user_id,
        ];

        $response = $this->postJson("/api/posts/{$post->id}", $data);

        $response->assertStatus(404);
    }

    /**
     * @test
     *
     * @throws
     *
     * @return void
     */
    public function successUpdatePosts()
    {
        Sanctum::actingAs(
            $this->user1,
            ['*']
        );

        $post = Posts::factory()
            ->create([
                'title' => Str::random(12),
                'description' => Str::random(20),
                'user_id' => $this->user1->id
            ]);

        $newTitle = Str::random(12);

        $data = [
            'title' => $newTitle,
            'description' => $post->description,
            'user_id' => $post->user_id,
        ];

        $response = $this->postJson("/api/posts/{$post->id}", $data);

        $response->assertStatus(200);

        $post->refresh();

        $post = $response->decodeResponseJson()['data'];

        $this->assertEquals($post['title'], $newTitle);
    }

    /**
     * @test
     *
     * @throws
     *
     * @return void
     */
    public function removePost()
    {
        Sanctum::actingAs(
            $this->user1,
            ['*']
        );

        $post = Posts::factory()
            ->create([
                'title' => Str::random(12),
                'description' => Str::random(12),
                'user_id' => $this->user1->id
            ]);

        $response = $this->call('DELETE', "/api/posts/{$post->id}");

        $response->assertStatus(200);

        $this->assertDatabaseMissing('posts', [
            'id' => $post->id,
            'user_id' => $this->user1->id
        ]);
    }

    /**
     * Prevent removing other user's post
     * @test
     *
     * @throws
     *
     * @return void
     */
    public function failRemovingPost()
    {
        Sanctum::actingAs(
            $this->user1,
            ['*']
        );

        $post = Posts::factory()
            ->create([
                'title' => Str::random(12),
                'description' => Str::random(12),
                'user_id' => $this->user2->id
            ]);

        $response = $this->call('DELETE', "/api/posts/{$post->id}");

        $response->assertStatus(404);

        $this->assertDatabaseHas('posts', [
            'id' => $post->id,
            'user_id' => $post->user_id
        ]);
    }
}
