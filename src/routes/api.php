<?php

use Illuminate\Support\Facades\Route;


Route::group(['namespace' => '\\App\\Http\\Controllers\\Api'], function () {

    Route::post('/auth', 'AuthController@login')->name('auth');

    Route::group(['middleware' => ['auth:sanctum']], function () {

        Route::get('/users', 'UsersController@index')->name('users-list');

        Route::prefix('posts')->group(function () {
            Route::get('/', 'PostsController@index')->name('posts-list');
            Route::get('/{id}', 'PostsController@view')->name('posts-view');
            Route::post('/', 'PostsController@create')->name('posts-create');
            Route::post('/{id}', 'PostsController@update')->name('posts-update');
            Route::delete('/{id}', 'PostsController@delete')->name('posts-delete');
        });

        Route::prefix('comments')->group(function () {
            Route::get('/', 'CommentsController@index')->name('comments-list');
            Route::get('/{id}', 'CommentsController@view')->name('comments-view');
            Route::post('/', 'CommentsController@create')->name('comments-create');
            Route::post('/{id}', 'CommentsController@update')->name('comments-update');
            Route::delete('/{id}', 'CommentsController@delete')->name('comments-delete');
        });
    });
});