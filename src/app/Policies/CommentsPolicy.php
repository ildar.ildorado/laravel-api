<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class CommentsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create models.
     *
     * @param  User $user
     * @param  int $userId
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user, int $userId)
    {
        return ($user->id === $userId) ? Response::allow() : Response::denyWithStatus(404);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User $user
     * @param  int $userId
     *
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, int $userId)
    {
        return ($user->id === $userId) ? Response::allow() : Response::denyWithStatus(404);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  User $user
     * @param  int  $userId
     *
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, int $userId)
    {
        return $user->id === $userId ? Response::allow() : Response::denyWithStatus(404);
    }
}
