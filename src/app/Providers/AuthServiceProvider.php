<?php

namespace App\Providers;

 use Illuminate\Support\Facades\Gate;
use App\Models\Comments;
use App\Models\Posts;
use App\Policies\CommentsPolicy;
use App\Policies\PostsPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        Posts::class => PostsPolicy::class,
        Comments::class => CommentsPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
