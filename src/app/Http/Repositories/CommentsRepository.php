<?php

namespace App\Http\Repositories;

use App\Models\Comments;


class CommentsRepository
{
    public function create(array $postData): Comments
    {
        $comments = new Comments();

        $comments->fill($postData);
        $comments->save();

        return $comments;
    }

    public function update(Comments $comments, array $postData): Comments
    {
        $comments->fill($postData);
        $comments->save();

        return $comments;
    }

    public function delete($comments): bool
    {
        return (bool) $comments->delete();
    }
}