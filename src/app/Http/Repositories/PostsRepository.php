<?php

namespace App\Http\Repositories;

use App\Models\Posts;


class PostsRepository
{
    public function create(array $postData): Posts
    {
        $posts = new Posts();

        $posts->fill($postData);
        $posts->save();

        return $posts;
    }

    public function update(Posts $posts, array $postData): Posts
    {
        $posts->fill($postData);
        $posts->save();

        return $posts;
    }

    public function delete($posts): bool
    {
        return (bool) $posts->delete();
    }
}