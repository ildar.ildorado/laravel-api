<?php

namespace App\Http\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;


class UserRepository
{
    public function getAuthUser(string $name, string $password) :?User
    {
        $user = (new User())->getUser($name);

        if (!is_null($user) && Hash::check($password, $user->password)) {
            return $user;
        }

        return null;
    }
}