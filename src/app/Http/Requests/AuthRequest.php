<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:25',
            'password' => 'required|string|min:6|max:20',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'username.required' => 'Field name is required',
            'username.min' => 'The length of the name must be at least 3 characters',
            'username.max' => 'The length of the name must not be more than 25 characters',
            'password.required' => 'Field password is required',
            'password.min' => 'The length of the password must be at least 6 characters',
            'password.max' => 'The length of the password must not be more than 20 characters',
        ];
    }
}
