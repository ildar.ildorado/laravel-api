<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentsRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'post_id' => 'required|integer',
            'user_id' => 'required|integer',
            'description' => 'required|string|min:6|max:3000',
        ];
    }

    public function messages(): array
    {
        return [
            'post_id.required' => 'Field post_id is required',
            'user_id.required' => 'Field user_id is required',
            'description.required' => 'Field description is required',
            'description.min' => 'The length of the description must be at least 10 characters',
            'description.max' => 'The length of the description must not be more than 3000 characters',
        ];
    }
}
