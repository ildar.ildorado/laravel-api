<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostsRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'title' => 'required|string|min:3|max:50',
            'description' => 'required|string|min:10|max:3000',
            'user_id' => 'required|integer',
        ];
    }

    public function messages(): array
    {
        return [
            'title.required' => 'Field title is required',
            'title.min' => 'The length of the title must be at least 3 characters',
            'title.max' => 'The length of the title must not be more than 50 characters',
            'description.required' => 'Field description is required',
            'description.min' => 'The length of the description must be at least 10 characters',
            'description.max' => 'The length of the description must not be more than 3000 characters',
            'user_id.required' => 'Field user_id is required',
        ];
    }
}
