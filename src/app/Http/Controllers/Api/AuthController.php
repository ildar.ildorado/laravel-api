<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Repositories\UserRepository;
use App\Http\Requests\AuthRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    use ThrottlesLogins;

    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function login(AuthRequest $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->lockoutResponse($request);
        }

        $user = $this->getUser($request);

        if (!is_null($user)) {
            return $this->loginResponse($request, $user);
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse();
    }

    protected function loginResponse(AuthRequest $request, User $user)
    {
        $this->clearLoginAttempts($request);

        return $this->authenticated($user);
    }

    public function generateToken(User $user): array
    {
        return [
            'token' => $user->createToken('auth-token')->plainTextToken,
        ];
    }

    protected function getUser(Request $request): ?User
    {
        $credentials = $this->credentials($request);

        return $this->userRepository->getAuthUser(
            $credentials['name'],
            $credentials['password']
        );
    }

    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    protected function username(): string
    {
        return 'name';
    }

    protected function authenticated($user): JsonResponse
    {
        return response()->json(
            [
                'success' => true,
                'status' => 200,
                'user' => new UserResource($user),
                'token' => $this->generateToken($user),
            ]
        );
    }

    /**
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendFailedLoginResponse(): JsonResponse
    {
        return response()->json(
            [
                'success' => false,
                'status' => 400,
                'error' => "Name or password does not match",
            ]
        );
    }

    /**
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function lockoutResponse(Request $request): JsonResponse
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        return response()->json(
            [
                'success' => false,
                'status' => 400,
                'error' => "Too many attempts to authenticate. Try again after $seconds seconds",
            ]
        );
    }
}
