<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Repositories\CommentsRepository;
use App\Http\Requests\CommentsRequest;
use App\Http\Resources\CommentsCollection;
use App\Http\Resources\CommentsResource;
use App\Models\Comments;
use App\Models\Posts;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    private $commentsRepository;

    public function __construct(CommentsRepository $commentsRepository)
    {
        $this->commentsRepository = $commentsRepository;
    }

    /**
     *@param Request $request
     *
     * @return CommentsCollection
     */
    public function index(Request $request): CommentsCollection
    {
        $comments = Comments::paginate($request->get('per_page') ?? null);

        return new CommentsCollection($comments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param CommentsRequest $request
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     *
     * @return CommentsResource
     */
    public function create(CommentsRequest $request): CommentsResource
    {
        $userId = (int) $request->post('user_id');

        $this->authorize('create', [Comments::class, $userId]);

        $comments = $this->commentsRepository->create($request->input());

        return new CommentsResource($comments);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return CommentsResource
     */
    public function view(int $id)
    {
        $comments = Comments::findOrFail($id);

        return new CommentsResource($comments);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CommentsRequest $request
     * @param  int  $id
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     *
     * @return CommentsResource
     */
    public function update(CommentsRequest $request, int $id): CommentsResource
    {
        $userId = (int) $request->post('user_id');

        $this->authorize('update', [Comments::class, $userId]);

        $comments = Comments::findOrFail($id);
        $comments = $this->commentsRepository->update($comments, $request->post());

        return new CommentsResource($comments);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     *
     * @return bool
     */
    public function delete($id)
    {
        $comments = Comments::findOrFail($id);

        $this->authorize('delete', [Comments::class, $comments->user_id]);

        return $this->commentsRepository->delete($comments);
    }
}
