<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Repositories\PostsRepository;
use App\Http\Requests\PostsRequest;
use App\Http\Resources\PostsCollection;
use App\Http\Resources\PostsResource;
use App\Models\Posts;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    private $postsRepository;

    public function __construct(PostsRepository $postsRepository)
    {
        $this->postsRepository = $postsRepository;
    }

    /**
     * @param  Request $request
     *
     * @return PostsCollection
     */
    public function index(Request $request): PostsCollection
    {
        $post = Posts::with('comments')
            ->paginate($request->get('per_page') ?? null);

        return new PostsCollection($post);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param PostsRequest $request
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     *
     * @return PostsResource
     */
    public function create(PostsRequest $request): PostsResource
    {
        $userId = (int) $request->post('user_id');
        $this->authorize('create', [Posts::class, $userId]);

        $post = $this->postsRepository->create($request->input());

        return new PostsResource($post);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return PostsResource
     */
    public function view(int $id)
    {
        $post = Posts::with('comments')
            ->where('id', '=', $id)
            ->first();

        return new PostsResource($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PostsRequest $request
     * @param  int $id
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     *
     * @return PostsResource
     */
    public function update(PostsRequest $request, int $id): PostsResource
    {
        $userId = (int) $request->post('user_id');

        $this->authorize('update', [Posts::class, $userId]);

        $post = Posts::findOrFail($id);
        $post = $this->postsRepository->update($post, $request->input());

        return new PostsResource($post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     *
     * @return bool
     */
    public function delete($id)
    {
        $post = Posts::findOrFail($id);

        $this->authorize('delete', [Posts::class, $post->user_id]);

        return $this->postsRepository->delete($post);
    }
}
