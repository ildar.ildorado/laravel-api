# Laravel API with Sanctum Auth.

## Steps

- `git@gitlab.com:ildar.ildorado/laravel-api.git` 
- Copy `.env.example` to `.env` 
- `docker-compose up -d --build` 
- `docker-compose exec app composer install` 
- `docker-compose exec app php artisan migrate` 
- `docker-compose exec app php artisan db:seed --class=UserSeeder` 
- `docker-compose exec app php artisan serve` 

Then authenticate via some user's credentials see UserSeeder

Default expiration time sets to 24 hours, you may change it in config/sanctum
`expiration => 24 * 60`, set null if you don't want it to expire.

Also in Console/Kernel I have added command for clearing expired tokens.
- Make `post` request from postman to `http://127.0.0.1:8080/api/auth` and get auth token.
- Then choose `Authorization` type `Bearer token` and paste `token`

## Tests

- Before running tests make `docker-compose exec app php artisan config:cache --env=testing` and
`docker-compose exec app php artisan migrate`, it will create test tables
- To run tests make `docker-compose exec app ./vendor/bin/phpunit` command
After testing change `config:cache` back with `docker-compose exec app php artisan config:cache --env=your-app-name`

## Examples of API requests
Use port from docker-compose settings, it may be different from the screens below

Auth
![auth](src/public/examples/api_auth.png?raw=true "Auth")

Users lists with posts and comments
![users-with-posts-and-comments](src/public/examples/api_users_list_with_posts_and_comments.png?raw=true "Users with posts and comments")

Posts with Comments
![posts-with-comments](src/public/examples/api_posts_with_comments.png?raw=true "Posts and comments")

Comments
![comments](src/public/examples/api_comment.png?raw=true "Comments")